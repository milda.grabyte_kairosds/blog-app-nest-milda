module.exports = {
  development: {
    dialect: 'postgres',
    host: 'dbpostgres',
    port: 5432,
    username: 'pguser',
    password: 'pguser',
    database: 'pgdb',
  },
  test: {
    dialect: 'postgres',
    host: 'dbpostgres',
    port: 5432,
    username: 'pguser',
    password: 'pguser',
    database: 'pgdb',
  },
  production: {
    dialect: 'postgres',
    host: 'dbpostgres',
    port: 5432,
    username: 'pguser',
    password: 'pguser',
    database: 'pgdb',
  },
};
