FROM node:12 as builder

COPY package*.json ./

RUN npm install
COPY . .
RUN npm run build

EXPOSE 3008
CMD ["npm", "run", "start"]