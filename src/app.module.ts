import 'reflect-metadata';
import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModel } from './infrastructure/schemas/user.schema';
import { AuthorModel } from './infrastructure/schemas/author.schema';
import { PostModel } from './infrastructure/schemas/post.schema';
import { CommentModel } from './infrastructure/schemas/comment.schema';
import { OffensiveWordModel } from './infrastructure/schemas/offensive-word.schema';
import { AuthController } from './infrastructure/controllers/auth.controller';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './infrastructure/middlewares/passport';
import { AuthorController } from './infrastructure/controllers/author.controller';
import { PostController } from './infrastructure/controllers/post.controller';
import { OffensiveWordController } from './infrastructure/controllers/offensive-word.controller';
import { UseCases } from './application/use-cases/use-cases.library';
import { Services } from './domain/services/services.library';
import { Repositories } from './infrastructure/repositories/repositories.library';

@Module({
  imports: [
    SequelizeModule.forRoot({
      dialect: 'postgres',
      host: process.env.POSTGRES_HOST,
      port: parseInt(process.env.POSTGRES_PORT),
      username: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
      sync: { force: true },
      synchronize: true,
      models: [UserModel],
    }),
    SequelizeModule.forFeature([UserModel]),
    MongooseModule.forRoot(
      `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`,
      {
        authSource: process.env.MONGO_AUTH_SOURCE,
        auth: {
          user: process.env.MONGO_AUTH_USER,
          password: process.env.MONGO_AUTH_PASSWORD,
        },
        useUnifiedTopology: true,
        useNewUrlParser: true,
      },
    ),
    MongooseModule.forFeature([
      { name: 'Author', schema: AuthorModel },
      { name: 'OffensiveWords', schema: OffensiveWordModel },
      { name: 'Posts', schema: PostModel },
      { name: 'Comments', schema: CommentModel },
    ]),
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: {
        expiresIn: '60d',
      },
    }),
  ],
  controllers: [
    AuthController,
    AuthorController,
    PostController,
    OffensiveWordController,
  ],
  providers: [JwtStrategy, ...UseCases, ...Services, ...Repositories],
})
export class AppModule {}
