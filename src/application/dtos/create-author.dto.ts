import { ApiProperty } from '@nestjs/swagger';
import { IsAlpha, IsEmail, IsNotEmpty } from 'class-validator';

export class CreateAuthorDTO {
  readonly id?: string;

  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  readonly authorName: string;

  @ApiProperty()
  @IsAlpha()
  @IsNotEmpty()
  readonly authorNickname: string;
}
