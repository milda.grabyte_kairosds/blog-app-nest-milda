import { ApiProperty } from '@nestjs/swagger';
import { IsAlphanumeric, IsNotEmpty, IsNumber } from 'class-validator';

export class CreateOffensiveWordDTO {
  readonly id?: string;

  @ApiProperty()
  @IsAlphanumeric()
  @IsNotEmpty()
  readonly word: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
  readonly level: number;
}
