import { ApiProperty } from '@nestjs/swagger';
import { IsAlphanumeric, IsNotEmpty } from 'class-validator';
import { Comment } from 'src/domain/model/entities/comment.entity';

export class CreatePostDTO {
  readonly postId?: string;

  readonly authorId: string;

  readonly authorName: string;

  readonly authorNickname: string;

  @ApiProperty()
  @IsAlphanumeric()
  @IsNotEmpty()
  readonly title: string;

  @ApiProperty()
  @IsAlphanumeric()
  @IsNotEmpty()
  readonly text: string;

  readonly commentList: Comment[];
}
