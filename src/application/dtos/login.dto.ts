import { ApiProperty } from '@nestjs/swagger';
import { IsAlphanumeric, IsEmail, IsNotEmpty } from 'class-validator';

export class LogInDTO {
  readonly id?: string;

  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  readonly email: string;

  @ApiProperty()
  @IsAlphanumeric()
  @IsNotEmpty()
  readonly password: string;

  role?: string;
}
