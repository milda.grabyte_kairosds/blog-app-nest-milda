import { ApiProperty } from '@nestjs/swagger';
import { IsAlphanumeric, IsNotEmpty } from 'class-validator';

export class UpdateCommentDTO {
  readonly commentId: string;

  readonly postId?: string;

  readonly authorId: string;

  readonly authorNickname: string;

  @ApiProperty()
  @IsAlphanumeric()
  @IsNotEmpty()
  readonly content: string;

  readonly timestamp: number;
}
