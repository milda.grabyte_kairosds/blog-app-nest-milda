import { ApiProperty } from '@nestjs/swagger';
import { IsAlphanumeric } from 'class-validator';
import { Comment } from 'src/domain/model/entities/comment.entity';

export class UpdatePostDTO {
  readonly postId?: string;

  readonly authorId: string;

  readonly authorName: string;

  readonly authorNickname: string;

  @ApiProperty()
  @IsAlphanumeric()
  readonly title?: string;

  @ApiProperty()
  @IsAlphanumeric()
  readonly text?: string;

  readonly commentList: Comment[];
}
