export type AuthorRequest = {
    authorId: string;
    authorName: string;
    authorNickname: string;
}