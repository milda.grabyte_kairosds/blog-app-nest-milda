export type AuthorResponse = {
    authorId: string;
    authorName: string;
    authorNickname: string;
}