import { Role } from 'src/domain/model/vos/user-vos/role.vo';

export type CommentAuthRequest = {
  postId: string;
  commentId: string;
  userId: string;
  userRole: Role;
};
