export type CommentUpdateRequest = {
    content: string;
}