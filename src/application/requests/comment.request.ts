export type CommentRequest = {
    postId: string;
    userId: string;
    content: string;
}