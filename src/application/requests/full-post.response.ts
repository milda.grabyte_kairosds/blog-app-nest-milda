export type FullPostResponse = {
    postId: string,
    authorId: string,
    authorName: string,
    authorNickname: string,
    title: string,
    text: string
    commentList: any[]
}