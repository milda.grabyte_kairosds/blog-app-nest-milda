export type OffensiveWordRequest = {
    id: string;
    word: string;
    level: number; //son tipos primitivos
}