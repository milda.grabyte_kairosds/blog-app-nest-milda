export type Response = {
    id: string,
    word: string,
    level: number
}