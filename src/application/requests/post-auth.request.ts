import { Role } from 'src/domain/model/vos/user-vos/role.vo';

export type PostAuthRequest = {
  postId: string;
  userId: string;
  userRole: Role;
};
