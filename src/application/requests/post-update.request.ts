export type PostUpdateRequest = {
    title: string;
    text: string;
}