export type PostRequest = {
    userId: string;
    title: string;
    text: string;
}