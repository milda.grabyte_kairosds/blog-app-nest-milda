export type PostResponse = {
    postId: string,
    authorId: string,
    authorName: string,
    authorNickname: string,
    title: string,
    text: string
}