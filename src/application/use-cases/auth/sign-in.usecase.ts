import { Injectable } from '@nestjs/common';
import { UserService } from '../../../domain/services/user.service';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from 'jsonwebtoken';
import { EmailVO } from 'src/domain/model/vos/user-vos/email.vo';
import { ExceptionWithCode } from 'src/domain/exception-with-code';
import { PasswordVO } from 'src/domain/model/vos/user-vos/password.vo';

@Injectable()
export class SignInUseCase {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async execute(request: SignInRequest): Promise<string | null> {
    const user = await this.userService.getByEmail(
      EmailVO.create(request.email),
    );
    if (!user) {
      throw new ExceptionWithCode(404, 'User not found');
    }
    const plainPassword = PasswordVO.create(request.password);
    const isValid = await this.userService.isValidPassword(plainPassword, user);
    if (isValid) {
      const payload: JwtPayload = { email: user.email.value };
      return this.jwtService.sign(payload);
    }
    return null;
  }
}

export type SignInRequest = {
  email: string;
  password: string;
};
