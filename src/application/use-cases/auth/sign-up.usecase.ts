import { Injectable } from '@nestjs/common';
import { User, UserType } from 'src/domain/model/entities/user.entity';
import { idVO } from 'src/domain/model/vos/entity-vos/id.vo';
import { EmailVO } from 'src/domain/model/vos/user-vos/email.vo';
import { PasswordVO } from 'src/domain/model/vos/user-vos/password.vo';
import { Role, RoleVO } from 'src/domain/model/vos/user-vos/role.vo';
import { UserService } from 'src/domain/services/user.service';

@Injectable()
export class SignUpUseCase {
  constructor(private userService: UserService) {}

  async execute(request: SignUpRequest): Promise<void> {
    const user: UserType = {
      id: idVO.create(),
      email: EmailVO.create(request.email),
      password: PasswordVO.create(request.password),
      role: RoleVO.create(Role.PUBLISHER),
    };
    await this.userService.persist(new User(user));
  }
}

export type SignUpRequest = {
  email: string;
  password: string;
};
