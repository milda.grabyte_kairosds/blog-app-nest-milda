import { Injectable } from '@nestjs/common';
import { AuthorResponse } from 'src/application/requests/author.response';
import { AuthorService } from 'src/domain/services/author.service';

@Injectable()
export class RecoverAuthorUseCase {
  constructor(private authorService: AuthorService) {}

  async execute(): Promise<AuthorResponse[]> {
    const fullList = await this.authorService.list();
    const filteredList = fullList.map((author) => {
      return {
        authorId: author.authorId.value,
        authorName: author.authorName.value,
        authorNickname: author.authorNickname.value,
      };
    });
    return filteredList;
  }
}
