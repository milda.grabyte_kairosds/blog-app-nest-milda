import { Injectable } from '@nestjs/common';
import { AuthorResponse } from 'src/application/requests/author.response';
import { IdRequest } from 'src/application/requests/id.request';
import { ExceptionWithCode } from 'src/domain/exception-with-code';
import { AuthorService } from 'src/domain/services/author.service';

Injectable();
export class RecoverOneAuthorUseCase {
  constructor(private authorService: AuthorService) {}

  async execute(authorId: IdRequest): Promise<AuthorResponse> {
    const result = await this.authorService.findById(authorId);
    if (result !== null) {
      return {
        authorId: result.authorId.value,
        authorName: result.authorName.value,
        authorNickname: result.authorNickname.value,
      };
    } else throw new ExceptionWithCode(404, `Id ${authorId} not found.`);
  }
}
