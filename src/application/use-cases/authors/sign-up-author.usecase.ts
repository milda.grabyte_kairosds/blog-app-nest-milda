import { Injectable } from '@nestjs/common';
import { AuthorRequest } from 'src/application/requests/author.request';
import { AuthorService } from '../../../domain/services/author.service';

@Injectable()
export class SignUpAuthorUseCase {
  constructor(private authorService: AuthorService) {}

  async execute(authorRequest: AuthorRequest): Promise<void> {
    await this.authorService.persist(authorRequest);
  }
}
