import { Injectable } from '@nestjs/common';
import { CommentRequest } from 'src/application/requests/comment.request';
import { PostService } from 'src/domain/services/post.service';

@Injectable()
export class CreateCommentUseCase {
  constructor(private postService: PostService) {}

  async execute(commentRequest: CommentRequest): Promise<void> {
    await this.postService.persistComment(commentRequest);
  }
}
