import { Injectable } from '@nestjs/common';
import { CommentAuthRequest } from 'src/application/requests/comment-auth.request';
import { PostService } from 'src/domain/services/post.service';

@Injectable()
export class DeleteOneCommentUseCase {
  constructor(private postService: PostService) {}

  async execute(commentAuthRequest: CommentAuthRequest): Promise<void> {
    await this.postService.deleteOneComment(commentAuthRequest);
  }
}
