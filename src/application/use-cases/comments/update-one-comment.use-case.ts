import { Injectable } from '@nestjs/common';
import { CommentAuthRequest } from 'src/application/requests/comment-auth.request';
import { CommentUpdateRequest } from 'src/application/requests/comment-update.request';
import { PostService } from 'src/domain/services/post.service';

@Injectable()
export class UpdateOneCommentUseCase {
  constructor(private postService: PostService) {}

  async execute(
    commentAuthRequest: CommentAuthRequest,
    commentUpdateRequest: CommentUpdateRequest,
  ): Promise<void> {
    await this.postService.updateComment(
      commentAuthRequest,
      commentUpdateRequest,
    );
  }
}
