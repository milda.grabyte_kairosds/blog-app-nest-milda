import { Injectable } from '@nestjs/common';
import { OffensiveWordNoIdRequest } from 'src/application/requests/offensive-word-no-id.request';
import { OffensiveWordService } from 'src/domain/services/offensive-word.service';

@Injectable()
export class CreateOffensiveWordUseCase {
  constructor(private offensiveWordService: OffensiveWordService) {}

  execute(offensiveWordRequest: OffensiveWordNoIdRequest): void {
    this.offensiveWordService.persist(offensiveWordRequest);
  }
}
