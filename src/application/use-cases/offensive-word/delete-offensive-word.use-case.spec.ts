// jest.mock('./../../infrastructure/repositories/offensive-word.repository.mongo', () => {
//     return {
//         OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
//             return {
//                 delete: jest.fn(),
//                 findById: jest.fn().mockImplementation(() =>
//                     new OffensiveWord({ id: idVO.createWithUUID('f2dd593e-af8e-4754-bed3-b42d2cfce636'), word: WordVO.create('Tested'), level: LevelVO.create(1) })
//                 )
//             };
//         })
//     };
// });

// import 'reflect-metadata';
// import Container from 'typedi';
// import { DeleteOffensiveWordUseCase} from './delete-offensive-word.use-case';
// import { OffensiveWordRepositoryMongo } from  '../../infrastructure/repositories/offensive-word.repository.mongo';
// import { IdRequest } from '../id.request';
// import { OffensiveWord } from '../../domain/model/entities/offensive-word.entity';
// import { idVO } from '../../domain/model/vos/entity-vos/id.vo';
// import { WordVO } from '../../domain/model/vos/offensive-word-vos/word.vo';
// import { LevelVO } from '../../domain/model/vos/offensive-word-vos/level.vo';

// describe('Delete offensive word', ()=> {
//     test('Delete offensive word by checking its ID', async() => {
//         const repository = new OffensiveWordRepositoryMongo();
//         Container.set('OffensiveWordRepository', repository);

//         const useCase = Container.get(DeleteOffensiveWordUseCase);
//         const id: IdRequest = 'f2dd593e-af8e-4754-bed3-b42d2cfce636';

//         await useCase.execute(id);
//         expect(repository.delete).toHaveBeenCalled();
//     });
// });
