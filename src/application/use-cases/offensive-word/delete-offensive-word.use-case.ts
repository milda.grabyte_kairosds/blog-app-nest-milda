import { Injectable } from '@nestjs/common';
import { IdRequest } from 'src/application/requests/id.request';
import { OffensiveWordService } from 'src/domain/services/offensive-word.service';

@Injectable()
export class DeleteOffensiveWordUseCase {
  constructor(private offensiveWordService: OffensiveWordService) {}

  async execute(id: IdRequest): Promise<void> {
    await this.offensiveWordService.delete(id);
  }
}
