// jest.mock('./../../infrastructure/repositories/offensive-word.repository.mongo', () => {
//     return {
//         OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
//             return {
//                 list: jest.fn().mockImplementation(() => [
//                     new OffensiveWord({id: idVO.create(), word: WordVO.create('Test'), level: LevelVO.create(3)})])
//             };
//         })
//     };
// });

// import 'reflect-metadata';
// import Container from 'typedi';
// import { validate } from 'uuid';
// import { OffensiveWord } from '../../domain/model/entities/offensive-word.entity';
// import { idVO } from '../../domain/model/vos/entity-vos/id.vo';
// import { LevelVO } from '../../domain/model/vos/offensive-word-vos/level.vo';
// import { WordVO } from '../../domain/model/vos/offensive-word-vos/word.vo';
// import { OffensiveWordRepositoryMongo } from '../../infrastructure/repositories/offensive-word.repository.mongo';
// import { RecoverOffensiveWordUseCase } from './recover-offensive-word.use-case';

// describe('List all offensive words', () => {

//     it('Should get all offensive words from repository', async () => {
//         const repository = new OffensiveWordRepositoryMongo();
//         Container.set('OffensiveWordRepository', repository);

//         const useCase = Container.get(RecoverOffensiveWordUseCase);
//         const offensiveWords = await useCase.execute();

//         expect(repository.list).toHaveBeenCalled();
//         expect(validate(offensiveWords[0].id)).toBe(true);
//         expect(offensiveWords[0].level).toBe(3);
//         expect(offensiveWords[0].word).toEqual('Test');
//     });
// });
