import { Injectable } from '@nestjs/common';
import { Response } from 'src/application/requests/offensive-word.response';
import { OffensiveWordService } from 'src/domain/services/offensive-word.service';

@Injectable()
export class RecoverOffensiveWordUseCase {
  constructor(private offensiveWordService: OffensiveWordService) {}

  async execute(): Promise<Array<Response>> {
    const fullList = await this.offensiveWordService.list();
    const filteredList = fullList.map((offensiveWord) => {
      return {
        id: offensiveWord.id.value,
        word: offensiveWord.word.value,
        level: offensiveWord.level.value,
      };
    });
    return filteredList;
  }
}
