// jest.mock('./../../infrastructure/repositories/offensive-word.repository.mongo', () => {
//     return {
//         OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
//             return {
//                 findById: jest.fn().mockImplementation(() => new OffensiveWord({ id: idVO.createWithUUID('e3fb8dfc-3061-48ab-b473-efabc8851ff3'), word: WordVO.create('Test'), level: LevelVO.create(3)}))
//             };
//         })
//     };
// });

// import 'reflect-metadata';
// import Container from 'typedi';
// import { OffensiveWord } from '../../domain/model/entities/offensive-word.entity';
// import { idVO } from '../../domain/model/vos/entity-vos/id.vo';
// import { LevelVO } from '../../domain/model/vos/offensive-word-vos/level.vo';
// import { WordVO } from '../../domain/model/vos/offensive-word-vos/word.vo';
// import { OffensiveWordRepositoryMongo } from '../../infrastructure/repositories/offensive-word.repository.mongo';
// import { RecoverOneOffensiveWordUseCase } from './recover-one-offensive-word.use-case';

// describe('Find one offensive word by its ID', () => {

//     it('Should get one offensive word from repository', async () => {
//         const repository = new OffensiveWordRepositoryMongo();
//         Container.set('OffensiveWordRepository', repository);

//         const useCase = Container.get(RecoverOneOffensiveWordUseCase);
//         const offensiveWord = await useCase.execute('e3fb8dfc-3061-48ab-b473-efabc8851ff3');
//         const uuid: idVO = idVO.createWithUUID('e3fb8dfc-3061-48ab-b473-efabc8851ff3');

//         expect(offensiveWord.id).toEqual('e3fb8dfc-3061-48ab-b473-efabc8851ff3');
//         expect(offensiveWord.word).toEqual('Test');
//         expect(offensiveWord.level).toBe(3);
//         expect(repository.findById).toHaveBeenCalledWith(uuid);
//     });
// });
