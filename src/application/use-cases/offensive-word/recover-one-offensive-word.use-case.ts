import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { Injectable } from '@nestjs/common';
import { IdRequest } from 'src/application/requests/id.request';
import { Response } from 'src/application/requests/offensive-word.response';

@Injectable()
export class RecoverOneOffensiveWordUseCase {
  constructor(private offensiveWordService: OffensiveWordService) {}

  async execute(id: IdRequest): Promise<Response> {
    const result = await this.offensiveWordService.findById(id);
    if (result !== null) {
      return {
        id: result.id.value,
        word: result.word.value,
        level: result.level.value,
      };
    } else throw new Error(`Id ${id} not found usecase`);
  }
}
