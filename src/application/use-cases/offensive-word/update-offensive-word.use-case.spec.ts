// jest.mock(
//   '../../infrastructure/repositories/offensive-word.repository.mongo',
//   () => {
//     return {
//       OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
//         return {
//           update: jest.fn(),
//           findById: jest.fn().mockImplementation(
//             () =>
//               new OffensiveWord({
//                 id: idVO.createWithUUID('17685407-d029-4547-8f2e-9189d65051f7'),
//                 word: WordVO.create('Test'),
//                 level: LevelVO.create(3),
//               }),
//           ),
//         };
//       }),
//     };
//   },
// );
// import 'reflect-metadata';
// import Container from 'typedi';
// import { OffensiveWordRepositoryMongo } from '../../infrastructure/repositories/offensive-word.repository.mongo';
// import { UpdateOffensiveWordUseCase } from './update-offensive-word.use-case';
// import { OffensiveWordRequest } from '../offensive-word.request';
// import { OffensiveWord } from '../../domain/model/entities/offensive-word.entity';
// import { idVO } from '../../domain/model/vos/entity-vos/id.vo';
// import { WordVO } from '../../domain/model/vos/offensive-word-vos/word.vo';
// import { LevelVO } from '../../domain/model/vos/offensive-word-vos/level.vo';

// describe('Update offensive word use case', () => {
//   it('Should update offensive word and persist', async () => {
//     const repository = new OffensiveWordRepositoryMongo();
//     Container.set('OffensiveWordRepository', repository);

//     const useCase = Container.get(UpdateOffensiveWordUseCase);
//     const offensiveWordRequest: OffensiveWordRequest = {
//       id: '17685407-d029-4547-8f2e-9189d65051f7',
//       word: 'Test',
//       level: 3,
//     };

//     await useCase.execute(offensiveWordRequest);
//     expect(repository.update).toHaveBeenCalled();
//   });
// });
