import { Injectable } from '@nestjs/common';
import { OffensiveWordRequest } from 'src/application/requests/offensive-word.request';
import { OffensiveWordService } from 'src/domain/services/offensive-word.service';

@Injectable()
export class UpdateOffensiveWordUseCase {
  constructor(private offensiveWordService: OffensiveWordService) {}

  async execute(offensiveWordRequest: OffensiveWordRequest): Promise<void> {
    await this.offensiveWordService.update(offensiveWordRequest);
  }
}
