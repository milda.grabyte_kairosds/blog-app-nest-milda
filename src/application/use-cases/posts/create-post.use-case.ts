import { Injectable } from '@nestjs/common';
import { PostRequest } from 'src/application/requests/post.request';
import { PostService } from '../../../domain/services/post.service';

@Injectable()
export class CreatePostUseCase {
  constructor(private postService: PostService) {}

  execute(postRequest: PostRequest): void {
    this.postService.persist(postRequest);
  }
}
