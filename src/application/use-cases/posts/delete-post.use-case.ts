import { Injectable } from '@nestjs/common';
import { PostAuthRequest } from 'src/application/requests/post-auth.request';
import { PostService } from 'src/domain/services/post.service';

@Injectable()
export class DeletePostUseCase {
  constructor(private postService: PostService) {}

  async execute(postAuthRequest: PostAuthRequest): Promise<void> {
    await this.postService.delete(postAuthRequest);
  }
}
