import { Injectable } from '@nestjs/common';
import { FullPostResponse } from 'src/application/requests/full-post.response';
import { IdRequest } from 'src/application/requests/id.request';
import { ExceptionWithCode } from 'src/domain/exception-with-code';
import { Comment } from 'src/domain/model/entities/comment.entity';
import { PostService } from 'src/domain/services/post.service';

@Injectable()
export class RecoverOnePostWithCommentsUseCase {
  constructor(private postService: PostService) {}

  async execute(id: IdRequest): Promise<FullPostResponse | null> {
    const result = await this.postService.findById(id);
    if (result !== null) {
      const comments = result.commentList.map((comment: Comment) => {
        return {
          commentId: comment.commentId.value,
          postId: comment.postId.value,
          authorId: comment.authorId.value,
          authorNickname: comment.authorNickname.value,
          content: comment.content.value,
          timestamp: comment.timestamp.value,
        };
      });
      return {
        postId: result.postId.value,
        authorId: result.authorId.value,
        authorName: result.authorName.value,
        authorNickname: result.authorNickname.value,
        title: result.title.value,
        text: result.text.value,
        commentList: comments,
      };
    } else throw new ExceptionWithCode(404, 'Post is not found');
  }
}
