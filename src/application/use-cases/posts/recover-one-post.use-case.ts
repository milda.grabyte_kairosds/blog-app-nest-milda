import { Injectable } from '@nestjs/common';
import { IdRequest } from 'src/application/requests/id.request';
import { PostResponse } from 'src/application/requests/post.response';
import { ExceptionWithCode } from 'src/domain/exception-with-code';
import { PostService } from 'src/domain/services/post.service';

@Injectable()
export class RecoverOnePostUseCase {
  constructor(private postService: PostService) {}

  async execute(id: IdRequest): Promise<PostResponse | null> {
    const result = await this.postService.findById(id);
    if (result !== null) {
      return {
        postId: result.postId.value,
        authorId: result.authorId.value,
        authorName: result.authorName.value,
        authorNickname: result.authorNickname.value,
        title: result.title.value,
        text: result.text.value,
      };
    } else throw new ExceptionWithCode(404, 'Post is not found');
  }
}
