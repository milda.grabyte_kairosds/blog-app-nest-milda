import { Injectable } from '@nestjs/common';
import { FullPostResponse } from 'src/application/requests/full-post.response';
import { Comment } from 'src/domain/model/entities/comment.entity';
import { PostService } from 'src/domain/services/post.service';

@Injectable()
export class RecoverPostWithCommentsUseCase {
  constructor(private postService: PostService) {}

  async execute(): Promise<Array<FullPostResponse>> {
    const fullList = await this.postService.list();
    const filteredList = fullList.map((post) => {
      const comments = post.commentList.map((comment: Comment) => {
        return {
          commentId: comment.commentId.value,
          postId: comment.postId.value,
          authorId: comment.authorId.value,
          authorNickname: comment.authorNickname.value,
          content: comment.content.value,
          timestamp: comment.timestamp.value,
        };
      });
      return {
        postId: post.postId.value,
        authorId: post.authorId.value,
        authorName: post.authorName.value,
        authorNickname: post.authorNickname.value,
        title: post.title.value,
        text: post.text.value,
        commentList: comments,
      };
    });
    return filteredList;
  }
}
