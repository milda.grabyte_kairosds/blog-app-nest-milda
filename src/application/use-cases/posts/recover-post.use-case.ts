import { Injectable } from '@nestjs/common';
import { PostResponse } from 'src/application/requests/post.response';
import { PostService } from 'src/domain/services/post.service';

@Injectable()
export class RecoverPostUseCase {
  constructor(private postService: PostService) {}

  async execute(): Promise<Array<PostResponse>> {
    const fullList = await this.postService.list();
    const filteredList = fullList.map((post) => {
      return {
        postId: post.postId.value,
        authorId: post.authorId.value,
        authorName: post.authorName.value,
        authorNickname: post.authorNickname.value,
        title: post.title.value,
        text: post.text.value,
      };
    });
    return filteredList;
  }
}
