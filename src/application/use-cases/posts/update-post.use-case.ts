import { Injectable } from '@nestjs/common';
import { PostAuthRequest } from 'src/application/requests/post-auth.request';
import { PostUpdateRequest } from 'src/application/requests/post-update.request';
import { PostService } from 'src/domain/services/post.service';

@Injectable()
export class UpdatePostUseCase {
  constructor(private postService: PostService) {}

  async execute(
    postUpdateRequest: PostUpdateRequest,
    postAuthRequest: PostAuthRequest,
  ): Promise<void> {
    await this.postService.update(postUpdateRequest, postAuthRequest);
  }
}
