import { SignInUseCase } from './auth/sign-in.usecase';
import { SignUpUseCase } from './auth/sign-up.usecase';
import { RecoverAuthorUseCase } from './authors/recover-author.use-case';
import { RecoverOneAuthorUseCase } from './authors/recover-one-author.use-case';
import { SignUpAuthorUseCase } from './authors/sign-up-author.usecase';
import { CreateCommentUseCase } from './comments/create-comment.use-case';
import { DeleteCommentUseCase } from './comments/delete-comment.use-case';
import { DeleteOneCommentUseCase } from './comments/delete-one-comment.use-case';
import { UpdateOneCommentUseCase } from './comments/update-one-comment.use-case';
import { CreateOffensiveWordUseCase } from './offensive-word/create-offensive-word.use-case';
import { DeleteOffensiveWordUseCase } from './offensive-word/delete-offensive-word.use-case';
import { RecoverOffensiveWordUseCase } from './offensive-word/recover-offensive-word.use-case';
import { RecoverOneOffensiveWordUseCase } from './offensive-word/recover-one-offensive-word.use-case';
import { UpdateOffensiveWordUseCase } from './offensive-word/update-offensive-word.use-case';
import { CreatePostUseCase } from './posts/create-post.use-case';
import { DeletePostUseCase } from './posts/delete-post.use-case';
import { RecoverOnePostWithCommentsUseCase } from './posts/recover-one-post-with-comments.use-case';
import { RecoverOnePostUseCase } from './posts/recover-one-post.use-case';
import { RecoverPostWithCommentsUseCase } from './posts/recover-post-with-comments.use-case';
import { RecoverPostUseCase } from './posts/recover-post.use-case';
import { UpdatePostUseCase } from './posts/update-post.use-case';

export const UseCases = [
  SignInUseCase,
  SignUpUseCase,
  RecoverAuthorUseCase,
  RecoverOneAuthorUseCase,
  SignUpAuthorUseCase,
  CreateCommentUseCase,
  DeleteCommentUseCase,
  DeleteOneCommentUseCase,
  UpdateOneCommentUseCase,
  CreateOffensiveWordUseCase,
  DeleteOffensiveWordUseCase,
  RecoverOffensiveWordUseCase,
  RecoverOneOffensiveWordUseCase,
  UpdateOffensiveWordUseCase,
  CreatePostUseCase,
  DeletePostUseCase,
  RecoverOnePostUseCase,
  RecoverOnePostWithCommentsUseCase,
  RecoverPostWithCommentsUseCase,
  RecoverPostUseCase,
  UpdatePostUseCase,
];
