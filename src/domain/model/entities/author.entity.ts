import { AuthorNicknameVO } from '../vos/author-vos/authorNickname.vo';
import { AuthorNameVO } from '../vos/author-vos/authorName.vo';
import { idVO } from '../vos/entity-vos/id.vo';

export type AuthorType = {
  authorId: idVO;
  authorNickname: AuthorNicknameVO;
  authorName: AuthorNameVO;
};

export class Author {
  constructor(private author: AuthorType) {}

  get authorId(): idVO {
    return this.author.authorId;
  }

  get authorNickname(): AuthorNicknameVO {
    return this.author.authorNickname;
  }

  get authorName(): AuthorNameVO {
    return this.author.authorName;
  }
}
