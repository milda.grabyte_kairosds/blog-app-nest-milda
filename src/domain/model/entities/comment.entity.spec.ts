import { ContentVO } from '../vos/comment-vos/content.vo';
import { idVO } from '../vos/entity-vos/id.vo';
import { TimestampVO } from '../vos/comment-vos/timestamp.vo';
import { AuthorNicknameVO } from '../vos/post-vos/authorNickname.vo';
import { Comment } from './comment.entity';

describe('Create a new comment and check its methods', () => {

    test('Should create a new valid comment and apply its methods', () => {

        const comment = new Comment({
            commentId: idVO.create(),
            authorId: idVO.create(),
            postId: idVO.createWithUUID('0448438b-064d-41ac-8448-d6bdcd4c0973'),
            authorNickname: AuthorNicknameVO.create('Fulanito'),
            content: ContentVO.create('No me gustan las palabrotas'),
            timestamp: TimestampVO.create()
        }); 

        const createdID = comment.commentId.value;
        const timestampString = comment.timestamp.value.toString();
        expect(createdID).toBeDefined();
        expect(idVO.createWithUUID(createdID)).toEqual({'id': comment.commentId.value});
        expect(comment.authorNickname.value).toBe('Fulanito');
        expect(comment.content.value).toBe('No me gustan las palabrotas');
        expect(comment.timestamp.value).toBeDefined();
        expect(timestampString.length).toBe(13);
    });
});