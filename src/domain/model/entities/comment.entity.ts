import { ContentVO } from '../vos/comment-vos/content.vo';
import { idVO } from '../vos/entity-vos/id.vo';
import { TimestampVO } from '../vos/comment-vos/timestamp.vo';
import { AuthorNicknameVO } from '../vos/author-vos/authorNickname.vo';

export type CommentType = {
  commentId: idVO;
  postId: idVO;
  authorId: idVO;
  authorNickname: AuthorNicknameVO;
  content: ContentVO;
  timestamp: TimestampVO;
};

export class Comment {
  constructor(private comment: CommentType) {}

  get commentId(): idVO {
    return this.comment.commentId;
  }

  get postId(): idVO {
    return this.comment.postId;
  }

  get authorId(): idVO {
    return this.comment.authorId;
  }

  get authorNickname(): AuthorNicknameVO {
    return this.comment.authorNickname;
  }

  get content(): ContentVO {
    return this.comment.content;
  }

  get timestamp(): TimestampVO {
    return this.comment.timestamp;
  }
}
