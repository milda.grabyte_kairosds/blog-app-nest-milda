import { idVO } from '../vos/entity-vos/id.vo';
import { LevelVO } from '../vos/offensive-word-vos/level.vo';
import { WordVO } from '../vos/offensive-word-vos/word.vo';
import { OffensiveWord } from './offensive-word.entity';


describe('Create a new offensive word and check its methods', () => {

    test('Should create a new valid offensive word and apply its methods', () => {

        const offensiveWord = new OffensiveWord({
            id: idVO.create(),
            word: WordVO.create('Word'),
            level: LevelVO.create(1)
        }); 

        const createdID = offensiveWord.id.value;
        expect(createdID).toBeDefined();
        expect(idVO.createWithUUID(createdID)).toEqual({'id': offensiveWord.id.value});
        expect(offensiveWord.word.value).toBe('Word');
        expect(offensiveWord.level.value).toBe(1);
    });
});