import { idVO } from '../vos/entity-vos/id.vo';
import { LevelVO } from '../vos/offensive-word-vos/level.vo';
import { WordVO } from '../vos/offensive-word-vos/word.vo';

export type OffensiveWordType = {
  id: idVO;
  word: WordVO;
  level: LevelVO;
};

export class OffensiveWord {
  constructor(private offensiveWord: OffensiveWordType) {} // toma tres parámetros

  get id(): idVO {
    return this.offensiveWord.id;
  }

  get word(): WordVO {
    return this.offensiveWord.word;
  }

  get level(): LevelVO {
    return this.offensiveWord.level;
  }
}
