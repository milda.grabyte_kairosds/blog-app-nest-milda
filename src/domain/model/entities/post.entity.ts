import { TextVO } from '../vos/post-vos/text.vo';
import { TitleVO } from '../vos/post-vos/title.vo';
import { AuthorNameVO } from '../vos/author-vos/authorName.vo';
import { AuthorNicknameVO } from '../vos/author-vos/authorNickname.vo';
import { Comment } from './comment.entity';
import { idVO } from '../vos/entity-vos/id.vo';

export type PostType = {
  postId: idVO;
  authorId: idVO;
  authorName: AuthorNameVO;
  authorNickname: AuthorNicknameVO;
  title: TitleVO;
  text: TextVO;
  commentList: Comment[];
};

export class Post {
  constructor(private post: PostType) {}

  get postId(): idVO {
    return this.post.postId;
  }

  get authorId(): idVO {
    return this.post.authorId;
  }

  get authorName(): AuthorNameVO {
    return this.post.authorName;
  }

  get authorNickname(): AuthorNicknameVO {
    return this.post.authorNickname;
  }

  get title(): TitleVO {
    return this.post.title;
  }

  get text(): TextVO {
    return this.post.text;
  }

  get commentList(): Comment[] {
    return this.post.commentList;
  }
}
