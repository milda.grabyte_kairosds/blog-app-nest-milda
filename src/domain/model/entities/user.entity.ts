import { EmailVO } from '../vos/user-vos/email.vo';
import { PasswordVO } from '../vos/user-vos/password.vo';
import { idVO } from '../vos/entity-vos/id.vo';
import { RoleVO } from '../vos/user-vos/role.vo';

export type UserType = {
  id: idVO;
  email: EmailVO;
  password: PasswordVO;
  role: RoleVO;
};

export class User {
  constructor(private user: UserType) {}

  get id(): idVO {
    return this.user.id;
  }

  get email(): EmailVO {
    return this.user.email;
  }

  get password(): PasswordVO {
    return this.user.password;
  }

  get role(): RoleVO {
    return this.user.role;
  }
}
