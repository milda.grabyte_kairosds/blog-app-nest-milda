import { AuthorNameVO } from './authorName.vo';

describe('AuthorNameVO', () => {
    test('Should create a new valid authorName', () => {
        const authorName1 = AuthorNameVO.create('A beautiful author name').value;
        expect(authorName1).toBe('A beautiful author name');
        expect(() => {authorName1;}).not.toThrow('A valid author name should be 5-30 characters long.');
    });

    test('Should throw a validation error', () => {
        const authorName2 = 'This author name should be a lot shorter than it is, but we live in an imperfect world';
        expect(() => AuthorNameVO.create(authorName2)).toThrow('A valid author name should be 5-30 characters long.');
    });

    test('Should throw a validation error', () => {
        const authorName3 = 'Nope';
        expect(() => AuthorNameVO.create(authorName3)).toThrow('A valid author name should be 5-30 characters long.');
    });
});