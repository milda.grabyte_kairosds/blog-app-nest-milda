export class AuthorNameVO {

    get value(): string {
        return this.authorName;
    }

    private constructor(private authorName: string) {}

    static create(authorName: string): AuthorNameVO {

        if (authorName.length < 5 || authorName.length > 30) {
            throw new Error('A valid author name should be 5-30 characters long.');
        } 

        return new AuthorNameVO(authorName);
    }
}