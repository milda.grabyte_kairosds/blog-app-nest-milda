import { AuthorNicknameVO } from './authorNickname.vo';

describe('AuthorNicknameVO', () => {
    test('Should create a new valid nickname', () => {
        const nickname1 = AuthorNicknameVO.create('Menganito').value;
        expect(nickname1).toBe('Menganito');
        expect(() => {nickname1;}).not.toThrow('A valid nickname should be 3-10 characters long.');
    });

    test('Should throw a validation error', () => {
        const nickname2 = 'FulanitoMenganito';
        expect(() => AuthorNicknameVO.create(nickname2).value).toThrow('A valid nickname should be 3-10 characters long.');
    });

    test('Should throw a validation error', () => {
        const nickname3 = 'Fu';
        expect(() => AuthorNicknameVO.create(nickname3)).toThrow('A valid nickname should be 3-10 characters long.');
    });
});