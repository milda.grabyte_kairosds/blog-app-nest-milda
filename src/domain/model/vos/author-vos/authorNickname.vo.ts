import { ExceptionWithCode } from 'src/domain/exception-with-code';

export class AuthorNicknameVO {
  static authorNickname: string;

  get value(): string {
    return this.authorNickname;
  }

  private constructor(private authorNickname: string) {}

  static create(authorNickname: string): AuthorNicknameVO {
    if (authorNickname.length < 3 || authorNickname.length > 10) {
      throw new ExceptionWithCode(
        400,
        'A valid nickname should be 3-10 characters long.',
      );
    }

    return new AuthorNicknameVO(authorNickname);
  }
}
