import { ContentVO } from './content.vo';

describe('Content VO', () => {
    test('Should create a new valid comment text', () => {
        const content = ContentVO.create('No me gusta este post.').value;
        expect(content).toBe('No me gusta este post.');
        expect(() => {content;}).not.toThrow('A comment should be at least 10 characters long.');
    });

    test('Should throw a validation error', () => {
        const content = 'A mí sí';
        expect(() => {ContentVO.create(content);}).toThrow('A comment should be at least 10 characters long.');
    });

    test('Should throw a validation error', () => {
        const content = 'qwertyuiopasdfghjklñzxcvbnnmqwertyuiopasdfghjllñzxcvbnmqwertyuiopasdfghjklñzxcvbnnmqwertyuiopasdfghjllñzxcvbnmqwertyuiopasdfghjklñzxcvbnnmqwertyuiopasdfghjllñzxcvbnmqwertyuiopasdfghjklñzxcvbnnmqwertyuiopasdfghjllñzxcvbnm';
        expect(() => {ContentVO.create(content);}).toThrow('A comment should not exceed 200 characters');
    });
});