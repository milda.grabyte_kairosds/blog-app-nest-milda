import { ExceptionWithCode } from 'src/domain/exception-with-code';
import { logger } from '../../../../infrastructure/config/logger';
import { matchOffensiveWords } from '../../../services/match-offensive-words-in-comment.service';
import { OffensiveWord } from '../../entities/offensive-word.entity';

export class ContentVO {
  get value(): string {
    return this.content;
  }

  private constructor(private content: string) {}

  static create(content: string, offensiveWordDB: OffensiveWord[]): ContentVO {
    const offensiveWordsMatched = matchOffensiveWords(content, offensiveWordDB);

    if (content.length < 10) {
      throw new ExceptionWithCode(
        400,
        'A comment should be at least 10 characters long.',
      );
    } else if (content.length > 200) {
      throw new ExceptionWithCode(
        400,
        'A comment should not exceed 200 characters',
      );
    } else if (offensiveWordsMatched.length > 0) {
      logger.debug(JSON.stringify(offensiveWordsMatched));
      throw new ExceptionWithCode(
        400,
        'Offensive words have been found. Please watch your language.',
      );
    }

    return new ContentVO(content);
  }
}
