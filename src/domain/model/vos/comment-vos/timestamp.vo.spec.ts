import { TimestampVO } from './timestamp.vo';

describe('Timestamp VO', () => {
    test('Should create a new valid timestamp',  () => {
        const timestamp = TimestampVO.create().value;
        const stringifiedTimestamp = timestamp.toString();
        
        expect(timestamp).toBeDefined();
        expect(timestamp).toEqual(expect.any(Number));
        expect(stringifiedTimestamp.length).toBe(13);
    });

    test('Should save existing timestamp',  () => {
        const newTimestamp = TimestampVO.create().value;
        const timestamp = TimestampVO.createWithTimestamp(newTimestamp).value;
        console.log(timestamp);
        const stringifiedTimestamp = timestamp.toString();
        
        expect(timestamp).toBeDefined();
        expect(timestamp).toEqual(expect.any(Number));
        expect(stringifiedTimestamp.length).toBe(13);
        expect(newTimestamp).toBe(timestamp);
    });
});