export class TimestampVO {

    get value(): number {
        return this.timestamp;
    }


    private constructor(private timestamp: number) {}

    static create(): TimestampVO {
        return new TimestampVO(Date.now()); // unix timestamp
    }

    static createWithTimestamp(timestamp: number): TimestampVO {
        return new TimestampVO (timestamp);
    }
}