import {idVO} from './id.vo';

describe('ID VO', () => {
    it('Should create a new UUID', () => {
        const uuid: idVO = idVO.create();
        expect(uuid.value).toBeDefined();
    });

    test('Should throw a validation error', () => {
        const uuid = 'e-45456-bt6h-oofensive';
        expect(() => {idVO.createWithUUID(uuid);}).toThrow('ID no es un UUID');
    });

    test('Should create and validate an uuid', ()=> {
        const uuid = idVO.create();
        const validatedID = idVO.createWithUUID(uuid.value);
        expect(validatedID).not.toEqual('ID no es un UUID');
    });
});