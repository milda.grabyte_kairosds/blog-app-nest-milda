import { ExceptionWithCode } from 'src/domain/exception-with-code';
import { v4 as uuidv4, validate } from 'uuid';

export class idVO {
  //un metodo para crearse
  get value(): string {
    return this.id;
  }

  private constructor(private id: string) {} // se puede meter un id por parámetro

  static create(): idVO {
    return new idVO(uuidv4()); // se puede crear un uuid desde cero y fijarlo como id
  }

  static createWithUUID(uuid: string): idVO {
    // se puede validar un uuid y, en el caso que lo sea, fijarlo como id
    if (!validate(uuid)) {
      throw new ExceptionWithCode(400, 'ID no es un UUID');
    }
    return new idVO(uuid);
  }
}
