import {WordVO} from './word.vo';

describe('Word VO', () => {
    it('Should create a new word', () => {
        const newWord = 'Word';
        const notAWord = 23;
        const word: WordVO = WordVO.create(newWord);
        expect(word.value).toEqual(newWord);
        expect(word.value).toStrictEqual(expect.any(String));
        expect(word.value).not.toEqual(notAWord);
    });
});