import { ExceptionWithCode } from 'src/domain/exception-with-code';

export class TextVO {
  get value(): string {
    return this.text;
  }

  private constructor(private text: string) {}

  static create(text: string): TextVO {
    function countWords(text: string): number {
      return text.split(' ').filter((word) => {
        return word !== '';
      }).length;
    }

    const wordCount = countWords(text);

    if (wordCount < 50) {
      throw new ExceptionWithCode(
        400,
        'A post should be at least 50 words long',
      );
    } else if (wordCount > 500) {
      throw new ExceptionWithCode(400, 'A post should not exceed 500 words');
    }

    return new TextVO(text);
  }
}
