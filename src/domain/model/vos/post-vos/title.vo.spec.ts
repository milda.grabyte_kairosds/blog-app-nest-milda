import { TitleVO } from './title.vo';

describe('TitleVO', () => {
    test('Should create a new valid title', () => {
        const title1 = TitleVO.create('A beautiful title').value;
        expect(title1).toBe('A beautiful title');
        expect(() => {title1;}).not.toThrow('A valid title should be 5-30 characters long.');
    });

    test('Should throw a validation error', () => {
        const title2 = 'This title should be a lot shorter than it is, but we live in an imperfect world';
        expect(() => TitleVO.create(title2)).toThrow('A valid title should be 5-30 characters long.');
    });

    test('Should throw a validation error', () => {
        const title3 = 'Nope';
        expect(() => TitleVO.create(title3)).toThrow('A valid title should be 5-30 characters long.');
    });
});