import { ExceptionWithCode } from 'src/domain/exception-with-code';

export class TitleVO {
  get value(): string {
    return this.title;
  }

  private constructor(private title: string) {}

  static create(title: string): TitleVO {
    if (title.length < 5 || title.length > 30) {
      throw new ExceptionWithCode(
        400,
        'A valid title should be 5-30 characters long.',
      );
    }

    return new TitleVO(title);
  }
}
