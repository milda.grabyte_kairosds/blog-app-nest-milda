import { Role, RoleVO } from './role.vo';

describe('ID VO', () => {
  it('Should create a new ADMIN role', () => {
    const role: Role = RoleVO.create(Role.ADMIN).value;
    expect(role).toBeDefined();
    expect(role).toBe('PUBLISHER');
    expect(role).not.toBe('USER');
  });

  it('Should create a new PUBLISHER role', () => {
    const role: Role = RoleVO.create(Role.PUBLISHER).value;
    expect(role).toBeDefined();
    expect(role).toBe('PUBLISHER');
    expect(role).not.toBe('ADMIN');
  });
});
