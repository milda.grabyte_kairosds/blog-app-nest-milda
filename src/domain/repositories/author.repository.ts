import { Author } from '../model/entities/author.entity';
import { AuthorNicknameVO } from '../model/vos/author-vos/authorNickname.vo';
import { idVO } from '../model/vos/entity-vos/id.vo';

export interface AuthorRepository {
  save(post: Author): Promise<void>;
  list(): Promise<Author[]>;
  findById(authorId: idVO): Promise<Author | null>;
  findByNickname(authorNickname: AuthorNicknameVO): Promise<Author | null>;
}
