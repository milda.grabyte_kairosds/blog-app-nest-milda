//va a definir los métodos (lo que recibe y devuelve), pero no va a implementar nada
import { OffensiveWord } from '../model/entities/offensive-word.entity';
import { idVO } from '../model/vos/entity-vos/id.vo';

export interface OffensiveWordRepository {
    save(offensiveWord: OffensiveWord): void;
    list(): Promise<OffensiveWord[]>;
    delete(id: idVO): Promise<void>;
    findById(offensiveWordId: idVO): Promise<OffensiveWord | null>;
    update(offensiveWord: OffensiveWord): Promise<void>;
}