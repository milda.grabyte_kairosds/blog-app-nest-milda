import { Post } from '../../domain/model/entities/post.entity';
import { Comment } from '../model/entities/comment.entity';
import { idVO } from '../model/vos/entity-vos/id.vo';


export interface PostRepository {
    save(post: Post): void;
    saveComment(comment: Comment): Promise<void>;
    list(): Promise<Post[]>;
    findById(postId: idVO): Promise<Post | null>;
    delete(id: idVO): Promise<void>;
    deleteComment(post: Post): Promise<void>;
    deleteOneComment(postUuid: idVO, commentUuid: idVO): Promise<void>;
    updateComment(comment: Comment): Promise<void>;
    update(post: Post): Promise<void>;
}