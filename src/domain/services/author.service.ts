import { Injectable } from '@nestjs/common';
import { AuthorRequest } from 'src/application/requests/author.request';
import { IdRequest } from 'src/application/requests/id.request';
import { NicknameRequest } from 'src/application/requests/nickname.request';
import { AuthorRepositoryMongo } from 'src/infrastructure/repositories/author.repository.mongo';
import { ExceptionWithCode } from '../exception-with-code';
import { Author, AuthorType } from '../model/entities/author.entity';
import { AuthorNameVO } from '../model/vos/author-vos/authorName.vo';
import { AuthorNicknameVO } from '../model/vos/author-vos/authorNickname.vo';
import { idVO } from '../model/vos/entity-vos/id.vo';

@Injectable()
export class AuthorService {
  constructor(private authorRepository: AuthorRepositoryMongo) {}

  async persist(authorRequest: AuthorRequest): Promise<void> {
    let authorDB = await this.findById(authorRequest.authorId);
    if (authorDB !== null)
      throw new ExceptionWithCode(409, 'This user is already an author.');
    authorDB = await this.findByNickname(authorRequest.authorNickname);
    if (authorDB !== null)
      throw new ExceptionWithCode(409, 'This nickname is already in use.');
    const newAuthor: AuthorType = {
      authorId: idVO.createWithUUID(authorRequest.authorId),
      authorName: AuthorNameVO.create(authorRequest.authorName),
      authorNickname: AuthorNicknameVO.create(authorRequest.authorNickname),
    };
    const authorEntity = new Author(newAuthor);
    await this.authorRepository.save(authorEntity);
  }

  async list(): Promise<Author[]> {
    return this.authorRepository.list();
  }

  async findById(authorId: IdRequest): Promise<Author | null> {
    const uuid = idVO.createWithUUID(authorId);
    return this.authorRepository.findById(uuid);
  }

  private async findByNickname(
    authorNickname: NicknameRequest,
  ): Promise<Author | null> {
    const nickname = AuthorNicknameVO.create(authorNickname);
    return this.authorRepository.findByNickname(nickname);
  }
}
