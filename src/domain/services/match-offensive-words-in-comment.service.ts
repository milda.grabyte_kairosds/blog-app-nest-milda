import { OffensiveWord } from '../model/entities/offensive-word.entity';

export const matchOffensiveWords = (
  content: string,
  offensiveWordDB: OffensiveWord[],
): OffensiveWord[] => {
  const punctuation = /[!"&'(),-.:;?]/g;
  const contentWords = content
    .toLowerCase()
    .replace(punctuation, '')
    .split(' ');
  const uniqueContentWords: string[] = [...Array.from(new Set(contentWords))];

  const offensiveWordsMatched: OffensiveWord[] = uniqueContentWords.reduce(
    (matchedWords: OffensiveWord[], contentWord: string): OffensiveWord[] => {
      offensiveWordDB.forEach((offensiveWord) => {
        if (
          offensiveWord.word.value.toLowerCase() === contentWord &&
          offensiveWord.level.value > 3
        ) {
          console.log(offensiveWord);
          matchedWords.push(offensiveWord);
        }
      });
      return matchedWords;
    },
    [],
  );
  return offensiveWordsMatched;
};
