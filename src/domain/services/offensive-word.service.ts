import { Injectable, OnModuleInit } from '@nestjs/common';
import { IdRequest } from 'src/application/requests/id.request';
import { OffensiveWordNoIdRequest } from 'src/application/requests/offensive-word-no-id.request';
import { OffensiveWordRequest } from 'src/application/requests/offensive-word.request';
import { OffensiveWordRepositoryMongo } from 'src/infrastructure/repositories/offensive-word.repository.mongo';
import { ExceptionWithCode } from '../exception-with-code';
import {
  OffensiveWord,
  OffensiveWordType,
} from '../model/entities/offensive-word.entity';
import { idVO } from '../model/vos/entity-vos/id.vo';
import { LevelVO } from '../model/vos/offensive-word-vos/level.vo';
import { WordVO } from '../model/vos/offensive-word-vos/word.vo';

@Injectable()
export class OffensiveWordService implements OnModuleInit {
  constructor(private offensiveWordRepository: OffensiveWordRepositoryMongo) {}

  persist(offensiveWordRequest: OffensiveWordNoIdRequest): void {
    const offensiveWordData: OffensiveWordType = {
      id: idVO.create(),
      word: WordVO.create(offensiveWordRequest.word),
      level: LevelVO.create(offensiveWordRequest.level),
    };
    const offensiveWordEntity = new OffensiveWord(offensiveWordData);
    this.offensiveWordRepository.save(offensiveWordEntity);
  }

  list(): Promise<OffensiveWord[]> {
    return this.offensiveWordRepository.list();
  }

  async delete(id: IdRequest): Promise<void> {
    await this.checkIfIDExists(id);
    const uuid = idVO.createWithUUID(id);
    this.offensiveWordRepository.delete(uuid);
  }

  async update(offensiveWord: OffensiveWordRequest): Promise<void> {
    const offensiveWordBefore = await this.checkIfIDExists(offensiveWord.id);
    console.log(offensiveWordBefore);
    const offensiveWordToUpdate: OffensiveWordType = {
      id: offensiveWordBefore.id,
      word: WordVO.create(offensiveWord.word ?? offensiveWordBefore.word.value),
      level: LevelVO.create(
        offensiveWord.level ?? offensiveWordBefore.level.value,
      ),
    };
    const newOffensiveWord = new OffensiveWord(offensiveWordToUpdate);
    this.offensiveWordRepository.update(newOffensiveWord);
  }

  async findById(offensiveWordId: IdRequest): Promise<OffensiveWord | null> {
    const uuid = idVO.createWithUUID(offensiveWordId);
    return this.offensiveWordRepository.findById(uuid);
  }

  private async checkIfIDExists(id: IdRequest): Promise<OffensiveWord> {
    const offensiveWord = await this.findById(id);
    if (offensiveWord === null) {
      throw new ExceptionWithCode(404, `Id ${id} not found`);
    }
    return offensiveWord;
  }

  async onModuleInit() {
    const offensiveWords = await this.list();
    if (offensiveWords.length == 0) {
      const offensiveWord1: OffensiveWordType = {
        id: idVO.create(),
        word: WordVO.create('puta'),
        level: LevelVO.create(5),
      };
      const offensiveWordEntity1 = new OffensiveWord(offensiveWord1);
      this.offensiveWordRepository.save(offensiveWordEntity1);

      const offensiveWord2: OffensiveWordType = {
        id: idVO.create(),
        word: WordVO.create('caca'),
        level: LevelVO.create(2),
      };
      const offensiveWordEntity2 = new OffensiveWord(offensiveWord2);
      this.offensiveWordRepository.save(offensiveWordEntity2);

      const offensiveWord3: OffensiveWordType = {
        id: idVO.create(),
        word: WordVO.create('mierda'),
        level: LevelVO.create(4),
      };
      const offensiveWordEntity3 = new OffensiveWord(offensiveWord3);
      this.offensiveWordRepository.save(offensiveWordEntity3);
    }
  }
}
