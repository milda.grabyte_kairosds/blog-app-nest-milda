import { Injectable } from '@nestjs/common';
import { CommentAuthRequest } from 'src/application/requests/comment-auth.request';
import { CommentUpdateRequest } from 'src/application/requests/comment-update.request';
import { CommentRequest } from 'src/application/requests/comment.request';
import { IdRequest } from 'src/application/requests/id.request';
import { PostAuthRequest } from 'src/application/requests/post-auth.request';
import { PostUpdateRequest } from 'src/application/requests/post-update.request';
import { PostRequest } from 'src/application/requests/post.request';
import { PostRepositoryMongo } from 'src/infrastructure/repositories/post.repository.mongo';
import { ExceptionWithCode } from '../exception-with-code';
import { Comment, CommentType } from '../model/entities/comment.entity';
import { OffensiveWord } from '../model/entities/offensive-word.entity';
import { Post, PostType } from '../model/entities/post.entity';
import { AuthorNicknameVO } from '../model/vos/author-vos/authorNickname.vo';
import { ContentVO } from '../model/vos/comment-vos/content.vo';
import { TimestampVO } from '../model/vos/comment-vos/timestamp.vo';
import { idVO } from '../model/vos/entity-vos/id.vo';
import { TextVO } from '../model/vos/post-vos/text.vo';
import { TitleVO } from '../model/vos/post-vos/title.vo';
import { AuthorService } from './author.service';
import { OffensiveWordService } from './offensive-word.service';

@Injectable()
export class PostService {
  constructor(
    private postRepository: PostRepositoryMongo,
    private offensiveWordService: OffensiveWordService,
    private authorService: AuthorService,
  ) {}

  async persist(postRequest: PostRequest): Promise<void> {
    const author = await this.authorService.findById(postRequest.userId);
    if (!author) {
      throw new ExceptionWithCode(404, 'User must sign up as an author first.');
    }
    const postData: PostType = {
      postId: idVO.create(),
      authorId: author.authorId,
      authorName: author.authorName,
      authorNickname: author.authorNickname,
      title: TitleVO.create(postRequest.title),
      text: TextVO.create(postRequest.text),
      commentList: [],
    };
    const postEntity = new Post(postData);
    this.postRepository.save(postEntity);
  }

  async persistComment(commentRequest: CommentRequest): Promise<void> {
    const author = await this.authorService.findById(commentRequest.userId);
    if (!author) {
      throw new ExceptionWithCode(404, 'User must sign up as an author first.');
    }
    const offensiveWordDB: OffensiveWord[] =
      await this.offensiveWordService.list();

    const newComment: CommentType = {
      commentId: idVO.create(),
      postId: idVO.createWithUUID(commentRequest.postId),
      authorId: author.authorId,
      authorNickname: author.authorNickname,
      content: ContentVO.create(commentRequest.content, offensiveWordDB),
      timestamp: TimestampVO.create(),
    };
    console.log(newComment.content);
    const comment = new Comment(newComment);
    await this.postRepository.saveComment(comment);
  }

  list(): Promise<Post[]> {
    return this.postRepository.list();
  }

  async delete(postAuthRequest: PostAuthRequest): Promise<void> {
    const post = await this.checkIfIDExists(postAuthRequest.postId);
    const postUuid = idVO.createWithUUID(postAuthRequest.postId);
    await this.canModify(postAuthRequest, post.authorId.value);

    this.postRepository.delete(postUuid);
  }

  async deleteComment(postAuthRequest: PostAuthRequest): Promise<void> {
    const postBefore = await this.checkIfIDExists(postAuthRequest.postId);
    await this.canModify(postAuthRequest, postBefore.authorId.value);

    const postToUpdate: PostType = {
      postId: postBefore.postId,
      authorId: postBefore.authorId,
      authorName: postBefore.authorName,
      authorNickname: postBefore.authorNickname,
      title: postBefore.title,
      text: postBefore.text,
      commentList: [],
    };
    const newPost = new Post(postToUpdate);
    this.postRepository.deleteComment(newPost);
  }

  async deleteOneComment(
    commentAuthRequest: CommentAuthRequest,
  ): Promise<void> {
    const postBefore = await this.checkIfIDExists(commentAuthRequest.postId);
    const commentListBefore = postBefore.commentList.map((comment) => {
      return {
        commentId: comment.commentId.value,
        postId: comment.postId.value,
        authorId: comment.authorId.value,
        authorNickname: comment.authorNickname.value,
        content: comment.content.value,
        timestamp: comment.timestamp.value,
      };
    });
    const result = commentListBefore.find(
      (comment) => comment.commentId === commentAuthRequest.commentId,
    );
    if (result === undefined) {
      throw new ExceptionWithCode(404, 'Comment not found');
    }
    const commentToDelete = result;

    await this.canModify(commentAuthRequest, commentToDelete.authorId);

    const postBeforeId = postBefore.postId;
    const commentToDeleteId = idVO.createWithUUID(commentToDelete.commentId);
    this.postRepository.deleteOneComment(postBeforeId, commentToDeleteId);
  }

  async updateComment(
    commentAuthRequest: CommentAuthRequest,
    commentUpdateRequest: CommentUpdateRequest,
  ): Promise<void> {
    const postBefore = await this.checkIfIDExists(commentAuthRequest.postId);

    const commentListBefore = postBefore.commentList.map((comment) => {
      return {
        commentId: comment.commentId.value,
        postId: comment.postId.value,
        authorId: comment.authorId.value,
        authorNickname: comment.authorNickname.value,
        content: comment.content.value,
        timestamp: comment.timestamp.value,
      };
    });
    const result = commentListBefore.find(
      (comment) => comment.commentId === commentAuthRequest.commentId,
    );
    if (result === undefined) {
      throw new ExceptionWithCode(404, 'Comment not found');
    }
    const commentBefore = result;

    await this.canModify(commentAuthRequest, commentBefore.authorId);

    const offensiveWordDB: OffensiveWord[] =
      await this.offensiveWordService.list();

    const commentToUpdate: CommentType = {
      commentId: idVO.createWithUUID(commentBefore.commentId),
      postId: idVO.createWithUUID(commentBefore.postId),
      authorId: idVO.createWithUUID(commentBefore.authorId),
      authorNickname: AuthorNicknameVO.create(commentBefore.authorNickname),
      content: ContentVO.create(
        commentUpdateRequest.content,
        offensiveWordDB ?? (commentBefore.content, []),
      ),
      timestamp: TimestampVO.createWithTimestamp(commentBefore.timestamp),
    };
    const newComment = new Comment(commentToUpdate);
    this.postRepository.updateComment(newComment);
  }

  async update(
    post: PostUpdateRequest,
    postAuthRequest: PostAuthRequest,
  ): Promise<void> {
    const postBefore = await this.checkIfIDExists(postAuthRequest.postId);
    await this.canModify(postAuthRequest, postBefore.authorId.value);

    const postToUpdate: PostType = {
      postId: postBefore.postId,
      authorId: postBefore.authorId,
      authorName: postBefore.authorName,
      authorNickname: postBefore.authorNickname,
      title: TitleVO.create(post.title ?? postBefore.title.value),
      text: TextVO.create(post.text ?? postBefore.text.value),
      commentList: postBefore.commentList,
    };
    const newPost = new Post(postToUpdate);
    this.postRepository.update(newPost);
  }

  async findById(id: IdRequest): Promise<Post | null> {
    const uuid = idVO.createWithUUID(id);
    return this.postRepository.findById(uuid);
  }

  private async checkIfIDExists(id: IdRequest): Promise<Post> {
    const post = await this.findById(id);
    if (post === null) {
      throw new ExceptionWithCode(404, 'Post not found');
    }
    return post;
  }

  private async canModify(
    authRequest: PostAuthRequest | CommentAuthRequest,
    entryAuthor: string,
  ): Promise<void> {
    const author = await this.authorService.findById(authRequest.userId);
    const isAdmin = authRequest.userRole === 'ADMIN';

    if (!author && !isAdmin) {
      throw new ExceptionWithCode(409, 'User must sign up as an author first.');
    } else if (author && !isAdmin && author.authorId.value !== entryAuthor) {
      throw new ExceptionWithCode(
        409,
        `Unauthorized: ${author.authorNickname.value} is not the author of this entry.`,
      );
    }
  }
}
