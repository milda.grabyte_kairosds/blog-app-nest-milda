import { Injectable, OnModuleInit } from '@nestjs/common';
import { hash, compare } from 'bcrypt';
import { UserRepositoryPG } from 'src/infrastructure/repositories/user.repository.pg';
import { User, UserType } from '../model/entities/user.entity';
import { idVO } from '../model/vos/entity-vos/id.vo';
import { EmailVO } from '../model/vos/user-vos/email.vo';
import { PasswordVO } from '../model/vos/user-vos/password.vo';
import { Role, RoleVO } from '../model/vos/user-vos/role.vo';

@Injectable()
export class UserService implements OnModuleInit {
  constructor(private userRepository: UserRepositoryPG) {}

  async isValidPassword(password: PasswordVO, user: User): Promise<boolean> {
    return compare(password.value, user.password.value);
  }

  async persist(user: User): Promise<void> {
    const passHash = await hash(user.password.value, 10);
    const encryptedPassword = PasswordVO.create(passHash);
    const newUser: UserType = {
      id: user.id,
      email: user.email,
      password: encryptedPassword,
      role: user.role,
    };
    await this.userRepository.save(new User(newUser));
  }

  async getByEmail(email: EmailVO): Promise<User | null> {
    return this.userRepository.getByEmail(email);
  }

  async onModuleInit() {
    const admin = await this.getByEmail(EmailVO.create('admin@example.org'));
    if (admin == null) {
      const passHash = await hash('123456789', 10);
      const encryptedPassword = PasswordVO.create(passHash);
      const newUser: UserType = {
        id: idVO.create(),
        email: EmailVO.create('admin@example.org'),
        password: encryptedPassword,
        role: RoleVO.create(Role.ADMIN),
      };
      this.userRepository.save(new User(newUser));
    }
  }
}
