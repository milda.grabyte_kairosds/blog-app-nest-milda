import { Body, Controller, Post, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { LogInDTO } from 'src/application/dtos/login.dto';
import { SignUpDTO } from 'src/application/dtos/sign-up.dto';
import {
  SignInRequest,
  SignInUseCase,
} from 'src/application/use-cases/auth/sign-in.usecase';
import {
  SignUpRequest,
  SignUpUseCase,
} from 'src/application/use-cases/auth/sign-up.usecase';

@Controller('api')
export class AuthController {
  constructor(
    private signInUseCase: SignInUseCase,
    private signUpUseCase: SignUpUseCase,
  ) {}

  @Post('sign-up')
  async signUp(
    @Body() body: SignUpDTO,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const { email, password } = body;
      const signUpRequest: SignUpRequest = { email, password };
      await this.signUpUseCase.execute(signUpRequest);
      res.status(201).json({ status: 'Created!' });
    } catch (err) {
      return res.status(err.code).json({ error: err.message });
    }
  }

  @Post('login')
  async logIn(
    @Body() body: LogInDTO,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const { email, password } = body;
      const signInRequest: SignInRequest = { email, password };
      const token = await this.signInUseCase.execute(signInRequest);
      if (token) {
        res.status(200).json({ token });
      } else {
        res.status(401).json({ error: 'Not permitted' });
      }
    } catch (err) {
      return res.status(err.code).json({ error: err.message });
    }
  }
}
