import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { logger } from '../config/logger';
import { User } from '../../domain/model/entities/user.entity';
// import { AuthGuard } from '@nestjs/passport';
// import { ApiBearerAuth } from '@nestjs/swagger';
import { AuthorRequest } from 'src/application/requests/author.request';
import { SignUpAuthorUseCase } from 'src/application/use-cases/authors/sign-up-author.usecase';
import { RecoverAuthorUseCase } from 'src/application/use-cases/authors/recover-author.use-case';
import { RecoverOneAuthorUseCase } from 'src/application/use-cases/authors/recover-one-author.use-case';
import { IdRequest } from 'src/application/requests/id.request';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';

import { Role } from 'src/domain/model/vos/user-vos/role.vo';
import { Roles } from '../middlewares/decorators/roles.decorator';
import { CreateAuthorDTO } from 'src/application/dtos/create-author.dto';

@Controller('/api/author')
export class AuthorController {
  constructor(
    private signUpAuthorUseCase: SignUpAuthorUseCase,
    private recoverAuthorUseCase: RecoverAuthorUseCase,
    private recoverOneAuthorUseCase: RecoverOneAuthorUseCase,
  ) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @ApiBearerAuth()
  async createAuthor(
    @Body() body: CreateAuthorDTO,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const authorRequest: AuthorRequest = {
        authorId: (req.user as User).id.value,
        authorName: body.authorName,
        authorNickname: body.authorNickname,
      };
      await this.signUpAuthorUseCase.execute(authorRequest);
      res.status(201).json({ status: 'Created' });
    } catch (err) {
      logger.error(err);
      return res.status(err.code).json({ error: err.message });
    }
  }

  @Get()
  async getAllAuthors(@Req() req: Request, @Res() res: Response) {
    const allAuthors = await this.recoverAuthorUseCase.execute();
    res.json(allAuthors);
  }

  @Get(':authorId')
  async getAuthor(
    @Param('authorId') id: IdRequest,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    const authorId = id;
    console.log(authorId);
    const result = await this.recoverOneAuthorUseCase.execute(authorId);
    res.json(result);
  }

  // router.delete('/api/offensive-word/:id',
  //     passport.authenticate('jwt', {session: false}), hasRole([Role.ADMIN]),
  //     async (req,res) => {
  //         try {
  //             const useCase = Container.get(DeleteOffensiveWordUseCase);
  //             const id: IdRequest = req.params.id;
  //             await useCase.execute(id);
  //             return res.status(201).send('Deleted!');
  //         } catch (err) {
  //             res.status(err.code).json({errors: err.message});
  //         }
  //     });
  //
  // router.put('/api/offensive-word/:id',
  //     passport.authenticate('jwt', {session: false}), hasRole([Role.ADMIN]),
  //     (req, res) => {
  //         const { word, level } = req.body;
  //         const id = req.params.id;
  //         const offensiveWordRequest: OffensiveWordRequest = {
  //             id, word, level
  //         };
  //         const useCase = Container.get(UpdateOffensiveWordUseCase);
  //         useCase.execute(offensiveWordRequest);
  //         return res.status(201).send('Updated!');
  //     });
}
