import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { OffensiveWordNoIdRequest } from 'src/application/requests/offensive-word-no-id.request';
import { CreateOffensiveWordUseCase } from 'src/application/use-cases/offensive-word/create-offensive-word.use-case';
import { RecoverOffensiveWordUseCase } from 'src/application/use-cases/offensive-word/recover-offensive-word.use-case';
import { RecoverOneOffensiveWordUseCase } from 'src/application/use-cases/offensive-word/recover-one-offensive-word.use-case';
import { IdRequest } from 'src/application/requests/id.request';
import { DeleteOffensiveWordUseCase } from 'src/application/use-cases/offensive-word/delete-offensive-word.use-case';
import { OffensiveWordRequest } from 'src/application/requests/offensive-word.request';
import { UpdateOffensiveWordUseCase } from 'src/application/use-cases/offensive-word/update-offensive-word.use-case';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';
import { Roles } from '../middlewares/decorators/roles.decorator';
import { Role } from 'src/domain/model/vos/user-vos/role.vo';
import { RolesGuard } from '../middlewares/guards/roles.guard';
import { CreateOffensiveWordDTO } from 'src/application/dtos/create-offensive-word.dto';
import { UpdateOffensiveWorddDTO } from 'src/application/dtos/update-offensive-word.dto';

@Controller('/api/offensive-word')
export class OffensiveWordController {
  constructor(
    private createOffensiveWordUseCase: CreateOffensiveWordUseCase,
    private recoverOffensiveWordUseCase: RecoverOffensiveWordUseCase,
    private recoverOneOffensiveWordUseCase: RecoverOneOffensiveWordUseCase,
    private deleteOffensiveWordUseCase: DeleteOffensiveWordUseCase,
    private updateOffensiveWordUseCase: UpdateOffensiveWordUseCase,
  ) {}

  @Post()
  @ApiBearerAuth()
  @Roles(Role.ADMIN)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async createOW(
    @Body() body: CreateOffensiveWordDTO,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    const { word, level } = body;
    const offensiveWordRequest: OffensiveWordNoIdRequest = {
      word,
      level,
    };
    await this.createOffensiveWordUseCase.execute(offensiveWordRequest);
    return res.status(201).send('Created!');
  }

  @Get()
  @ApiBearerAuth()
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async getAllOW(@Req() req: Request, @Res() res: Response) {
    const allOffensiveWords = await this.recoverOffensiveWordUseCase.execute();
    res.json(allOffensiveWords);
  }

  @Get(':id')
  @ApiBearerAuth()
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async getOneOW(
    @Param('id') WordId: IdRequest,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    const id = WordId;
    const result = await this.recoverOneOffensiveWordUseCase.execute(id);
    res.json(result);
  }

  @Delete(':id')
  @ApiBearerAuth()
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async deleteOW(
    @Param('id') wordId: IdRequest,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const id = wordId;
      await this.deleteOffensiveWordUseCase.execute(id);
      return res.status(201).send('Deleted!');
    } catch (err) {
      res.status(err.code).json({ errors: err.message });
    }
  }

  @Put(':id')
  @ApiBearerAuth()
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async updateOW(
    @Param('id') wordId: IdRequest,
    @Body() body: UpdateOffensiveWorddDTO,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const id = wordId;
      const { word, level } = body;
      const offensiveWordRequest: OffensiveWordRequest = {
        id,
        word,
        level,
      };
      await this.updateOffensiveWordUseCase.execute(offensiveWordRequest);
      return res.status(201).send('Updated!');
    } catch (err) {
      res.status(err.code).json({ errors: err.message });
    }
  }
}
