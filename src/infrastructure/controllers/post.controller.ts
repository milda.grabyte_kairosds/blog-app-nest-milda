import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { CreateCommentDTO } from 'src/application/dtos/create-comment.dto';
import { CreatePostDTO } from 'src/application/dtos/create-post.dto';
import { UpdateCommentDTO } from 'src/application/dtos/update-comment.dto';
import { UpdatePostDTO } from 'src/application/dtos/update-post.dto';
import { CommentAuthRequest } from 'src/application/requests/comment-auth.request';
import { CommentUpdateRequest } from 'src/application/requests/comment-update.request';
import { CommentRequest } from 'src/application/requests/comment.request';
import { IdRequest } from 'src/application/requests/id.request';
import { PostAuthRequest } from 'src/application/requests/post-auth.request';
import { PostUpdateRequest } from 'src/application/requests/post-update.request';
import { PostRequest } from 'src/application/requests/post.request';
import { CreateCommentUseCase } from 'src/application/use-cases/comments/create-comment.use-case';
import { DeleteCommentUseCase } from 'src/application/use-cases/comments/delete-comment.use-case';
import { DeleteOneCommentUseCase } from 'src/application/use-cases/comments/delete-one-comment.use-case';
import { UpdateOneCommentUseCase } from 'src/application/use-cases/comments/update-one-comment.use-case';
import { CreatePostUseCase } from 'src/application/use-cases/posts/create-post.use-case';
import { DeletePostUseCase } from 'src/application/use-cases/posts/delete-post.use-case';
import { RecoverOnePostWithCommentsUseCase } from 'src/application/use-cases/posts/recover-one-post-with-comments.use-case';
import { RecoverOnePostUseCase } from 'src/application/use-cases/posts/recover-one-post.use-case';
import { RecoverPostWithCommentsUseCase } from 'src/application/use-cases/posts/recover-post-with-comments.use-case';
import { RecoverPostUseCase } from 'src/application/use-cases/posts/recover-post.use-case';
import { UpdatePostUseCase } from 'src/application/use-cases/posts/update-post.use-case';
import { User } from 'src/domain/model/entities/user.entity';
import { Role } from 'src/domain/model/vos/user-vos/role.vo';
import { Roles } from '../middlewares/decorators/roles.decorator';
import { RolesGuard } from '../middlewares/guards/roles.guard';

@Controller('/api/post')
export class PostController {
  constructor(
    private createPostUseCase: CreatePostUseCase,
    private recoverPostUseCase: RecoverPostUseCase,
    private createCommentUseCase: CreateCommentUseCase,
    private recoverPostWithCommentsUseCase: RecoverPostWithCommentsUseCase,
    private recoverOnePostUseCase: RecoverOnePostUseCase,
    private recoverOnePostWithCommentsUseCase: RecoverOnePostWithCommentsUseCase,
    private deletePostUseCase: DeletePostUseCase,
    private updatePostUseCase: UpdatePostUseCase,
    private deleteCommentUseCase: DeleteCommentUseCase,
    private updateOneCommentUseCase: UpdateOneCommentUseCase,
    private deleteOneCommentUseCase: DeleteOneCommentUseCase,
  ) {}

  @Post('')
  @ApiBearerAuth()
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async createPost(
    @Body() body: CreatePostDTO,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const userId = (req.user as User).id.value;
      const { text, title } = body;
      const postRequest: PostRequest = {
        userId,
        title,
        text,
      };
      await this.createPostUseCase.execute(postRequest);
      return res.status(201).send('Created!');
    } catch (err) {
      res.status(err.code).json({ errors: err.message });
    }
  }

  @Get()
  @ApiBearerAuth()
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async getAllPostsWithoutComments(@Req() req: Request, @Res() res: Response) {
    try {
      const allPosts = await this.recoverPostUseCase.execute();
      res.json(allPosts);
    } catch (err) {
      res.status(err.code).json({ errors: err.message });
    }
  }

  @Post(':postId/comment')
  @ApiBearerAuth()
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async createComment(
    @Param('postId') id: IdRequest,
    @Body() body: CreateCommentDTO,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      console.log(1);
      const userId = (req.user as User).id.value;
      const postId: IdRequest = id;
      const { content } = body;
      const commentRequest: CommentRequest = {
        userId,
        postId,
        content,
      };

      await this.createCommentUseCase.execute(commentRequest);
      return res.status(201).send('Created!');
    } catch (err) {
      res.status(err.code).json({ errors: err.message });
    }
  }

  @Get('comment')
  @ApiBearerAuth()
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async getAllPostsWithComments(@Req() req: Request, @Res() res: Response) {
    try {
      const allPostsWithComments =
        await this.recoverPostWithCommentsUseCase.execute();
      res.json(allPostsWithComments);
    } catch (err) {
      res.status(err.code).json({ errors: err.message });
    }
  }

  @Get(':postId')
  @ApiBearerAuth()
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async getOnePostWithoutComments(
    @Param('postId') postId: IdRequest,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const id = postId;
      const result = await this.recoverOnePostUseCase.execute(id);
      res.json(result);
    } catch (err) {
      res.status(err.code).json({ errors: err.message });
    }
  }

  @Get(':postId/comment')
  @ApiBearerAuth()
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async getOnePostWithComments(
    @Param('postId') postId: IdRequest,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const id = postId;
      const result = await this.recoverOnePostWithCommentsUseCase.execute(id);
      res.json(result);
    } catch (err) {
      res.status(err.code).json({ errors: err.message });
    }
  }

  @Delete(':postId')
  @ApiBearerAuth()
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async deletePost(
    @Param('postId') postId: IdRequest,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const postAuthRequest: PostAuthRequest = {
        postId: postId,
        userId: (req.user as User).id.value,
        userRole: (req.user as User).role.value,
      };
      await this.deletePostUseCase.execute(postAuthRequest);
      return res.status(201).send('Deleted!');
    } catch (err) {
      res.status(err.code).json({ errors: err.message });
    }
  }

  @Put(':postId')
  @ApiBearerAuth()
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async updatePost(
    @Param('postId') id: IdRequest,
    @Body() body: UpdatePostDTO,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const { title, text } = body;
      const postUpdateRequest: PostUpdateRequest = {
        title,
        text,
      };
      const postAuthRequest: PostAuthRequest = {
        postId: id,
        userId: (req.user as User).id.value,
        userRole: (req.user as User).role.value,
      };
      await this.updatePostUseCase.execute(postUpdateRequest, postAuthRequest);
      return res.status(201).send('Post updated!');
    } catch (err) {
      res.status(err.code).json({ errors: err.message });
    }
  }

  @Put(':postId/comment')
  @ApiBearerAuth()
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async deleteAllComments(
    @Param('postId') id: IdRequest,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const postAuthRequest: PostAuthRequest = {
        postId: id,
        userId: (req.user as User).id.value,
        userRole: (req.user as User).role.value,
      };
      await this.deleteCommentUseCase.execute(postAuthRequest);
      return res.status(201).send('All comments deleted!');
    } catch (err) {
      res.status(err.code).json({ errors: err.message });
    }
  }

  @Put(':postId/:commentId')
  @ApiBearerAuth()
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async updateComment(
    @Param('postId') pId: IdRequest,
    @Param('commentId') cId: IdRequest,
    @Body() body: UpdateCommentDTO,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const commentAuthRequest: CommentAuthRequest = {
        postId: pId,
        commentId: cId,
        userId: (req.user as User).id.value,
        userRole: (req.user as User).role.value,
      };
      const { content } = body;
      const updateCommentRequest: CommentUpdateRequest = {
        content,
      };
      await this.updateOneCommentUseCase.execute(
        commentAuthRequest,
        updateCommentRequest,
      );
      return res.status(201).send('Comment updated!');
    } catch (err) {
      res.status(err.code).json({ errors: err.message });
    }
  }

  @Put(':postId/:commentId/delete')
  @ApiBearerAuth()
  @Roles(Role.ADMIN, Role.PUBLISHER)
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  async deleteComment(
    @Param('postId') pId: IdRequest,
    @Param('commentId') cId: IdRequest,
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const commentAuthRequest: CommentAuthRequest = {
        postId: pId,
        commentId: cId,
        userId: (req.user as User).id.value,
        userRole: (req.user as User).role.value,
      };
      await this.deleteOneCommentUseCase.execute(commentAuthRequest);
      return res.status(201).send('Comment deleted!');
    } catch (err) {
      res.status(err.code).json({ errors: err.message });
    }
  }
}
