import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ExceptionWithCode } from 'src/domain/exception-with-code';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();

    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      return true;
    }
    const user = request.user;
    console.log(roles.some((role) => role === user.role.value));
    if (roles.some((role) => role === user.role.value)) {
      return true;
    } else throw new ExceptionWithCode(403, 'Unauthorized');
  }
}
