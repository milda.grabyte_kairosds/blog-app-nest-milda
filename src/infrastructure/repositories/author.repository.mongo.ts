import { InjectModel } from '@nestjs/mongoose';
import { AnyObject, Model } from 'mongoose';
import { Author, AuthorType } from 'src/domain/model/entities/author.entity';
import { AuthorNameVO } from 'src/domain/model/vos/author-vos/authorName.vo';
import { AuthorNicknameVO } from 'src/domain/model/vos/author-vos/authorNickname.vo';
import { idVO } from 'src/domain/model/vos/entity-vos/id.vo';
import { AuthorRepository } from 'src/domain/repositories/author.repository';
import { AuthorDocument } from '../schemas/author.schema';

export class AuthorRepositoryMongo implements AuthorRepository {
  constructor(
    @InjectModel('Author') private authorModel: Model<AuthorDocument>,
  ) {}

  async save(author: Author): Promise<void> {
    const newAuthor = {
      authorId: author.authorId.value,
      authorName: author.authorName.value,
      authorNickname: author.authorNickname.value,
    };
    console.log(newAuthor);
    const authorModel = new this.authorModel(newAuthor);
    await authorModel.save();
  }

  async list(): Promise<Author[]> {
    const response = await this.authorModel.find({}).exec();
    const authors = response.map((author: AnyObject) => {
      //mongo no tiene tipos
      const authorData: AuthorType = {
        authorId: idVO.createWithUUID(author.authorId),
        authorName: AuthorNameVO.create(author.authorName),
        authorNickname: AuthorNicknameVO.create(author.authorNickname),
      };
      return new Author(authorData);
    });
    return authors;
  }

  async findById(authorId: idVO): Promise<Author | null> {
    const authorDB = await this.authorModel.findOne({
      authorId: authorId.value,
    });
    console.log(authorDB);
    if (authorDB !== null) {
      const foundAuthor: AuthorType = {
        authorId: idVO.createWithUUID(authorDB.authorId),
        authorName: AuthorNameVO.create(authorDB.authorName),
        authorNickname: AuthorNicknameVO.create(authorDB.authorNickname),
      };
      return new Author(foundAuthor);
    } else return null;
  }

  async findByNickname(
    authorNickname: AuthorNicknameVO,
  ): Promise<Author | null> {
    const authorDB = await this.authorModel.findOne({
      authorNickname: authorNickname.value,
    });
    if (authorDB !== null) {
      const foundAuthor: AuthorType = {
        authorId: idVO.createWithUUID(authorDB.authorId),
        authorName: AuthorNameVO.create(authorDB.authorName),
        authorNickname: AuthorNicknameVO.create(authorDB.authorNickname),
      };
      return new Author(foundAuthor);
    } else return null;
  }
}
