import { AnyObject, Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import {
  OffensiveWord,
  OffensiveWordType,
} from 'src/domain/model/entities/offensive-word.entity';
import { OffensiveWordDocument } from '../schemas/offensive-word.schema';
import { OffensiveWordRepository } from 'src/domain/repositories/offensive-word.repository';
import { InjectModel } from '@nestjs/mongoose';
import { idVO } from 'src/domain/model/vos/entity-vos/id.vo';
import { WordVO } from 'src/domain/model/vos/offensive-word-vos/word.vo';
import { LevelVO } from 'src/domain/model/vos/offensive-word-vos/level.vo';

@Injectable()
export class OffensiveWordRepositoryMongo implements OffensiveWordRepository {
  constructor(
    @InjectModel('OffensiveWords')
    private offensiveWordModel: Model<OffensiveWordDocument>,
  ) {}
  save(offensiveWord: OffensiveWord): void {
    const newOffensiveWord = {
      id: offensiveWord.id.value,
      word: offensiveWord.word.value,
      level: offensiveWord.level.value,
    };
    const offensiveWordModel = new this.offensiveWordModel(newOffensiveWord);
    offensiveWordModel.save();
  }

  async list(): Promise<OffensiveWord[]> {
    const response = await this.offensiveWordModel.find({}).exec();
    const offensiveWords = response.map((foundWord: AnyObject) => {
      const offensiveWordData: OffensiveWordType = {
        id: idVO.createWithUUID(foundWord.id),
        word: WordVO.create(foundWord.word),
        level: LevelVO.create(foundWord.level),
      };
      return new OffensiveWord(offensiveWordData);
    });
    return offensiveWords;
  }

  async delete(uuid: idVO): Promise<void> {
    const id = uuid.value;
    const mongooseID = await this.offensiveWordModel.find({ id }).exec(); //findOne
    await this.offensiveWordModel.findByIdAndDelete(mongooseID);
  }

  async update(offensiveWord: OffensiveWord): Promise<void> {
    await this.offensiveWordModel.findOneAndUpdate(
      { id: offensiveWord.id.value },
      {
        word: offensiveWord.word.value,
        level: offensiveWord.level.value,
      },
    );
  }

  async findById(offensiveWordId: idVO): Promise<OffensiveWord | null> {
    const offensiveWordDB = await this.offensiveWordModel.findOne({
      id: offensiveWordId.value,
    });
    if (offensiveWordDB !== null) {
      const foundWord: OffensiveWordType = {
        id: idVO.createWithUUID(offensiveWordDB.id),
        word: WordVO.create(offensiveWordDB.word),
        level: LevelVO.create(offensiveWordDB.level),
      };
      return new OffensiveWord(foundWord);
    } else return null;
  }
}
