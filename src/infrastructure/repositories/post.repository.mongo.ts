import { AnyObject, Model } from 'mongoose';
import { Comment } from '../../domain/model/entities/comment.entity';
import { Post, PostType } from '../../domain/model/entities/post.entity';
import { ContentVO } from '../../domain/model/vos/comment-vos/content.vo';
import { TimestampVO } from '../../domain/model/vos/comment-vos/timestamp.vo';
import { idVO } from '../../domain/model/vos/entity-vos/id.vo';
import { AuthorNameVO } from '../../domain/model/vos/author-vos/authorName.vo';
import { AuthorNicknameVO } from '../../domain/model/vos/author-vos/authorNickname.vo';
import { TextVO } from '../../domain/model/vos/post-vos/text.vo';
import { TitleVO } from '../../domain/model/vos/post-vos/title.vo';
import { PostRepository } from '../../domain/repositories/post.repository';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PostDocument } from '../schemas/post.schema';

@Injectable()
export class PostRepositoryMongo implements PostRepository {
  constructor(
    @InjectModel('Posts')
    private postModel: Model<PostDocument>,
  ) {}
  save(post: Post): void {
    const newPost = {
      postId: post.postId.value,
      authorId: post.authorId.value,
      authorName: post.authorName.value,
      authorNickname: post.authorNickname.value,
      title: post.title.value,
      text: post.text.value,
      commentList: [],
    };
    const postModel = new this.postModel(newPost);
    postModel.save();
  }

  async saveComment(comment: Comment): Promise<void> {
    const commentToAdd = {
      commentId: comment.commentId.value,
      postId: comment.postId.value,
      authorId: comment.authorId.value,
      authorNickname: comment.authorNickname.value,
      content: comment.content.value,
      timestamp: comment.timestamp.value,
    };
    await this.postModel.findOneAndUpdate(
      { postId: comment.postId.value },
      { $push: { commentList: commentToAdd } },
    );
  }

  async list(): Promise<Post[]> {
    const response = await this.postModel.find({}).exec();
    const posts = response.map((foundPost: AnyObject) => {
      const comments = foundPost.commentList.map((comment: AnyObject) => {
        return new Comment({
          commentId: idVO.createWithUUID(comment.commentId),
          postId: idVO.createWithUUID(comment.postId),
          authorId: idVO.createWithUUID(comment.authorId),
          authorNickname: AuthorNicknameVO.create(comment.authorNickname),
          content: ContentVO.create(comment.content, []),
          timestamp: TimestampVO.createWithTimestamp(comment.timestamp),
        });
      });
      const postData: PostType = {
        postId: idVO.createWithUUID(foundPost.postId),
        authorId: idVO.createWithUUID(foundPost.authorId),
        authorNickname: AuthorNicknameVO.create(foundPost.authorNickname),
        authorName: AuthorNameVO.create(foundPost.authorName),
        title: TitleVO.create(foundPost.title),
        text: TextVO.create(foundPost.text),
        commentList: comments,
      };
      return new Post(postData);
    });
    return posts;
  }

  async delete(uuid: idVO): Promise<void> {
    const postId = uuid.value;
    const mongooseID = await this.postModel.find({ postId }).exec();
    await this.postModel.findByIdAndDelete(mongooseID);
  }

  async deleteComment(post: Post): Promise<void> {
    await this.postModel.updateOne(
      { postId: post.postId.value },
      { commentList: post.commentList },
    );
  }

  async deleteOneComment(postUuid: idVO, commentUuid: idVO): Promise<void> {
    const postId = postUuid.value;
    const commentId = commentUuid.value;
    await this.postModel.updateOne(
      { postId: postId },
      {
        $pull: { commentList: { commentId: commentId } },
      },
    );
  }

  async updateComment(comment: Comment): Promise<void> {
    const postId = comment.postId.value;
    const commentId = comment.commentId.value;
    const commentToUpdate = {
      commentId: commentId,
      postId: postId,
      authorId: comment.authorId.value,
      authorNickname: comment.authorNickname.value,
      content: comment.content.value,
      timestamp: comment.timestamp.value,
    };
    await this.postModel.updateOne(
      {
        postId: postId,
        'commentList.commentId': commentId,
      },
      {
        $set: { 'commentList.$': commentToUpdate },
      },
    );
  }

  async update(post: Post): Promise<void> {
    await this.postModel.updateOne(
      { postId: post.postId.value },
      {
        title: post.title.value,
        text: post.text.value,
      },
    );
  }

  async findById(postId: idVO): Promise<Post | null> {
    const postDB = await this.postModel.findOne({
      postId: postId.value,
    });
    if (postDB !== null) {
      const comments = postDB.commentList.map((comment: AnyObject) => {
        return new Comment({
          commentId: idVO.createWithUUID(comment.commentId),
          postId: idVO.createWithUUID(comment.postId),
          authorId: idVO.createWithUUID(comment.authorId),
          authorNickname: AuthorNicknameVO.create(comment.authorNickname),
          content: ContentVO.create(comment.content, []),
          timestamp: TimestampVO.createWithTimestamp(comment.timestamp),
        });
      });
      const foundPost: PostType = {
        postId: idVO.createWithUUID(postDB.postId),
        authorId: idVO.createWithUUID(postDB.authorId),
        authorNickname: AuthorNicknameVO.create(postDB.authorNickname),
        authorName: AuthorNameVO.create(postDB.authorName),
        title: TitleVO.create(postDB.title),
        text: TextVO.create(postDB.text),
        commentList: comments,
      };
      return new Post(foundPost);
    } else return null;
  }
}
