import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { User, UserType } from 'src/domain/model/entities/user.entity';
import { idVO } from 'src/domain/model/vos/entity-vos/id.vo';
import { EmailVO } from 'src/domain/model/vos/user-vos/email.vo';
import { PasswordVO } from 'src/domain/model/vos/user-vos/password.vo';
import { RoleVO } from 'src/domain/model/vos/user-vos/role.vo';
import { UserRepository } from 'src/domain/repositories/user.repository';
import { UserModel } from '../schemas/user.schema';

@Injectable()
export class UserRepositoryPG implements UserRepository {
  constructor(@InjectModel(UserModel) private userModel: typeof UserModel) {}
  async save(user: User): Promise<void> {
    const id = user.id.value;
    const email = user.email.value;
    const password = user.password.value;
    const role = user.role.value;

    const userModel = this.userModel.build({ id, email, password, role });
    await userModel.save();
  }

  async getByEmail(email: EmailVO): Promise<User | null> {
    const user: any = await UserModel.findOne({
      where: { email: email.value },
    });
    if (!user) {
      return null;
    }

    const userData: UserType = {
      id: idVO.createWithUUID(user.id),
      email: EmailVO.create(user.email),
      password: PasswordVO.create(user.password),
      role: RoleVO.create(user.role),
    };

    return new User(userData);
  }
}
