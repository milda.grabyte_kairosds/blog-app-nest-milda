import { Prop, SchemaFactory, Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type AuthorDocument = AuthorSchema & Document;

@Schema({ collection: 'Author' })
export class AuthorSchema {
  @Prop({
    type: String,
    required: true,
  })
  authorId: string;

  @Prop({
    type: String,
    required: true,
  })
  authorNickname: string;

  @Prop({
    type: String,
    required: true,
  })
  authorName: string;
}

export const AuthorModel = SchemaFactory.createForClass(AuthorSchema);
