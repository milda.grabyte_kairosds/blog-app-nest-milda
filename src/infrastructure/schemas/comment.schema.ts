import { Prop, SchemaFactory, Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type CommentDocument = CommentSchema & Document;

@Schema({ collection: 'Posts' })
export class CommentSchema {
  @Prop({
    type: String,
    required: true,
  })
  commentId: string;

  @Prop({
    type: String,
    required: true,
  })
  postId: string;

  @Prop({
    type: String,
    required: true,
  })
  authorId: string;

  @Prop({
    type: String,
    required: true,
  })
  authorNickname: string;

  @Prop({
    type: String,
    required: true,
  })
  content: string;

  @Prop({
    type: String,
    required: true,
  })
  timestamp: string;
}

export const CommentModel = SchemaFactory.createForClass(CommentSchema);
