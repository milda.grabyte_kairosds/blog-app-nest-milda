import { Prop, SchemaFactory, Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type OffensiveWordDocument = OffensiveWordSchema & Document;

@Schema({ collection: 'OffensiveWords' })
export class OffensiveWordSchema {
  @Prop({
    type: String,
    required: true,
  })
  id: string;

  @Prop({
    type: String,
    required: true,
  })
  word: string;

  @Prop({
    type: Number,
    required: true,
  })
  level: number;
}

export const OffensiveWordModel =
  SchemaFactory.createForClass(OffensiveWordSchema);
