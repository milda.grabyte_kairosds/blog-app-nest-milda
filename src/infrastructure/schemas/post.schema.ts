import { Prop, SchemaFactory, Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Comment } from 'src/domain/model/entities/comment.entity';

export type PostDocument = PostSchema & Document;

@Schema({ collection: 'Posts' })
export class PostSchema {
  @Prop({
    type: String,
    required: true,
  })
  postId: string;

  @Prop({
    type: String,
    required: true,
  })
  authorId: string;

  @Prop({
    type: String,
    required: true,
  })
  authorName: string;

  @Prop({
    type: String,
    required: true,
  })
  authorNickname: string;

  @Prop({
    type: String,
    required: true,
  })
  title: string;

  @Prop({
    type: String,
    required: true,
  })
  text: string;

  @Prop([
    {
      commentId: String,
      postId: String,
      authorId: String,
      authorNickname: String,
      content: String,
      timestamp: Number,
    },
  ])
  commentList: Comment[];
}

export const PostModel = SchemaFactory.createForClass(PostSchema);
